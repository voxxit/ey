package main

import (
	"os"

	"gitlab.com/voxxit/ey/command"

	"github.com/codegangsta/cli"
)

var app *cli.App

func main() {
	// Create a new app instance
	app = cli.NewApp()

	// Define the app we're building
	app.Author = "Joshua Delsman"
	app.Usage = "CLI tool to manange your Engine Yard infrastructure"
	app.Email = "j@srv.im"
	app.Name = "ey"
	app.Version = "0.1.0"
	app.Commands = []cli.Command{
		cli.Command{
			Name:   "login",
			Usage:  "Login/verify access to Engine Yard Cloud",
			Action: command.Login,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name: "email",
				},
				cli.StringFlag{
					Name: "password",
				},
			},
		},
		cli.Command{
			Name:   "logout",
			Usage:  "Removes any previously-saved Engine Yard Cloud access credentials",
			Action: command.Logout,
		},
	}

	// Run the app, with the given arguments
	app.Run(os.Args)
}
