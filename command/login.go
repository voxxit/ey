package command

import (
	"os"

	"gopkg.in/yaml.v1"

	"gitlab.com/voxxit/ey/api"

	"github.com/codegangsta/cli"
	"github.com/mitchellh/go-homedir"
)

// Login ...
func Login(c *cli.Context) {
	loginResponse, err := api.Login(&api.LoginRequest{
		Email:    c.String("email"),
		Password: c.String("password"),
	})
	if err != nil {
		panic(err)
	}

	out, err := yaml.Marshal(loginResponse)
	if err != nil {
		panic(err)
	}

	home, err := homedir.Dir()
	if err != nil {
		panic(err)
	}

	f, err := os.Create(home + "/.eyrc")
	if err != nil {
		panic(err)
	}

	defer f.Close()

	_, err = f.Write(out)
	if err != nil {
		panic(err)
	}
}
