package api

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

const (
	apiEndpoint = "https://cloud.engineyard.com/api/v2"
)

func postJSON(path string, in interface{}, out interface{}) error {
	// Encode the input as JSON
	enc, err := json.Marshal(&in)
	if err != nil {
		return err
	}

	// Make a POST request
	resp, err := http.Post(apiEndpoint+path, "application/json", bytes.NewReader(enc))
	if err != nil {
		return err
	}

	// Don't close the body until its been fully read
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	// Decode the JSON response
	json.Unmarshal(body, &out)

	return nil
}

// Login ...
func Login(in *LoginRequest) (out *LoginResponse, err error) {
	err = postJSON("/authenticate", &in, &out)
	if err != nil {
		return &LoginResponse{}, err
	}

	return out, nil
}
