package api

// LoginRequest ...
type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// LoginResponse ...
type LoginResponse struct {
	APIToken string `json:"api_token" yaml:"api_token"`
}
